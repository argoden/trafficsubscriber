######################################################################
#
#       @# makefile
#
#       All Rights Reserved.
#
#       Copyright (c) Spireon
#       http://www.spireon.com/
#
#       This Module contains Proprietary Information
#       and should be treated as Confidential.
#
#  Created: 2014-04-12
#  UUID: VERNUMB 
#
######################################################################
DESTDIR=$(HOME)
PROG_DIR=$(DESTDIR)/bin
CONF_DIR=$(DESTDIR)


PROGS= rabbitmqadmin \
       trafficSubscriber \
       setup_listeners.sh \
       list_traffic_queues

DATAS= cron_cmd

CONFS= rabbitmqadmin.conf

all:     $(PROGS) $(CONFS)  
install: $(CRON) 

$(PROGS): dummy
	mkdir -p $(PROG_DIR)
	cp $@ $(PROG_DIR)
	chmod 775 $(PROG_DIR)/$@

$(CONFS): dummy
	mkdir -p $(CONF_DIR)
	cp $@ $(CONF_DIR)
	mv $(CONF_DIR)/$@ $(CONF_DIR)/.$@

$(CRON): dummy
	$(crontab < $@)

clobber:
	for i in $(PROGS); do \
	(echo rm -f $(PROG_DIR)/.$$i); \
	(rm -f $(PROG_DIR)/.$$i); \
	done;
	for i in $(CONFS); do \
	(echo rm -f $(CONF_DIR)/.$$i); \
	(rm -f $(CONF_DIR)/.$$i); \
	done;

clean:
dummy:
install:

