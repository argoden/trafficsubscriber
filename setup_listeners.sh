#!/usr/bin/env bash

init(){
    if [ -f "./externalConfig" ]; then
        echo "Reading from ./externalConfig"
        . ./externalConfig
    else
        echo "Error! ./externalConfig can not be found" 
        exit
    fi
}

send_url() {
    curl --header 'Content-Type: application/x-www-form-urlencoded' --data $1 $SERVER_URL:$SERVER_PORT/console/execute
}

#for acct in ${ACCOUNT_IDS[@]}; do
#echo -e "Deactivating listeners\n"
#send_url "code=ctx.getBean('eventListenerService').deactivate()"
#done

#for dev in ${DEVICE_IDS[@]}; do
#echo -e "\nSetting up listener for device $dev"
#send_url "code=ctx.getBean('eventListenerService').activate(device:${dev})"
#done


##############################
# MAIN
##############################
init
for acct in ${ACCOUNT_IDS[@]}; do
echo -e "\nSetting up listener for account $acct"
send_url "code=ctx.getBean('eventListenerService').activate(account:${acct})"
done
